#Gulp workflow for SASS

You must have installed on your computer:
- [git](https://git-scm.com/downloads)
- [node.js](https://nodejs.org/download/)
- [gulp](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)

Test if they work
```
$ git --version
$ npm -v
$ gulp -v
```

1) Configuring the environment

- Open Git Bash
- Create a project folder
```
$ mkdir /d/project
```
- Copy files from repo into the project folder

2) Install gulp dependencies

- Open Git Bash
- Open the project folder
```
$ cd /d/project
```
- Run
```
$ npm install
```
or 
```
$ npm i
```
It will install all gulp dependencies

2) Download curl latest version

- Go to the [link](http://www.paehl.com/open_source/?Welcome)
- Click on CURL in the sidebar
- Download **WITH SUPPORT SSL**
- Extract files to any folder

e.g.
```
d:\project\
```

If you have 32bit system you need to install openssl and Microsoft Visual C++ 2010 SP1 as well

3) Project settings

- Rename settings-example.json into settings.json
- Write your settings in the fields

### Getting started

1) Creating the partials.json for correct work with SASS upload

- Rename partials-example.json into partials.json
- Run 'gulp partials'
```
$ gulp partials
```

2) Simple work

There are 2 main tasks

- gulp watch

- gulp prod


###gulp watch

- watches input folder for any changes in scss files

- compiles css, 

- uploads compiled css file to sandbox

- injects compiled css to the page

###gulp prod

this task we run only when

- development process on the jira ticket is over

- we are ready to make a commit to the GIT repository


What does the gulp prod task actually do?

- creates sass documentation using sassdoc

- compiles css files

- adds a vendor prefixes

- formats the css code using csscomb

- outputs css files to the output folder

Note: you have to manually upload files to the sandbox (e.g. using Eclipse) after prod task