'use strict';

// -----------------------------------------------------------------------------
// Dependencies
// -----------------------------------------------------------------------------
const settings = require('./settings.json'),
    partials = require('./partials.json'),
    gulp = require('gulp-param')(require('gulp'), process.argv),
    watch = require('gulp-watch'),
    path  = require('path'),
    notify = require("gulp-notify"),
    notifier = require('node-notifier'),
    plumber = require('gulp-plumber'),
    shell = require('gulp-shell'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    csscomb = require('gulp-csscomb'),
    sassdoc = require('sassdoc'),
    data = require('gulp-data'),
    fm = require('front-matter'),
    gutil = require('gulp-util'),
    through2 = require('through2'),
    spritesmith = require('gulp.spritesmith'),
    merge = require('merge-stream');

// -----------------------------------------------------------------------------
// Configuration
// -----------------------------------------------------------------------------
const configuration = {
    input: null, 
    inputPath: null,
    sassPathSrc: null,
    sassPathDest: null,
    sassPathDok: null,
    watchPaths: [],
    sassPaths: [],
    partials: {},
    partialsJSON: 'partials.json',
    notifyInfo: {
        title: 'Adidas project',
        icon: path.join(__dirname, 'gulp.png'),
        errorMessage: 'Error: <%= error.message %>',
        compiledMessage: '<%= file.relative %> is compited!',
        createdMessage: '<%= file.relative %> is created!'
    },
    autoprefixerOptions: {
        browsers: [
            'last 2 Chrome versions',
            'last 4 Android versions',
            'last 3 iOS versions',
            'last 2 OperaMobile versions',
            'last 4 ChromeAndroid versions',
            'last 2 FirefoxAndroid versions',
            'last 2 ExplorerMobile versions'
        ]
    },
    webdav: {
        auth: settings.sandbox.username + ':' + settings.sandbox.password,
        path: 'https://'+ settings.sandbox.domen + '/on/demandware.servlet/webdav/Sites/Cartridges/' + settings.sandbox.cartridge + '/'
    },
    message: function(fileName, task, actions) {
        var str;
        if (task == 'upload') {
            str = 'File ' + fileName + ' was ' + actions;
        } else {
            str = 'File ' + fileName + ' was ' + actions + ', running task: "' + task + '"';
        }
        return str;
    },
    init: function() {
        this.events();
    },
    events: function() {
        this.buildWatchPaths();
        this.buildSassPaths();
    },
    lastElement: function(str, sep) {
        return str.split(sep).pop();
    },
    currentCartridge: function(str, sep) {
        var i, iLength,
            arr = str.split(sep),
            cartridge = null;

        for (i = 0, iLength = arr.length; i < iLength; i++) {
            if (arr[i] === 'cartridges') {
                cartridge = arr[i + 1];
            }
        }
        return cartridge;
    },
    messageFn: function(message, title) {
        notifier.notify(configuration.notifyHandler(message, title));
        console.log(message);
    },
    uploadAction: function(e, evt) {
        var path = e,
            fileName = this.lastElement(path, '\\'),
            cartridge = this.currentCartridge(path, '\\'),
            fileNameExtension = this.lastElement(fileName, '.'),
            pathToFolder = settings.watch[fileNameExtension].foldedPath;
        
        this.inputPath = cartridge + pathToFolder;
        this.input = path;

        gulp.start('upload');

        if (evt == 'change') {
            this.messageFn(this.message(fileName, 'upload', 'changed'), cartridge);
        } else {
            this.messageFn(this.message(fileName, 'upload', 'added'), cartridge);
        }
    },
    sassAction: function(e, evt) {
        var path = e,
            fileName = this.lastElement(path, '\\'),
            cartridge = this.currentCartridge(path, '\\'),
            pathToFolder = settings.sass.path.dest;

        path = this.sassPartials(path, cartridge, partials[cartridge]);

        this.sassPathSrc = settings.projectPath + cartridge + settings.sass.path.src + settings.sass.extension;
        this.sassPathDest = settings.projectPath + cartridge + settings.sass.path.dest;
        this.inputPath = cartridge + pathToFolder;
        this.input = path.replace(/scss/g, 'css');

        gulp.start('sass-upload');

        this.messageFn(this.message(fileName, 'sass', 'changed'), cartridge);
    },
    // Checking the partial scss of the global scss
    sassPartials: function(input, cartridge, arr) {
        var i, ilength, j, jlength, parent, child, mess,
            path = '';

        for (i = 0, ilength = arr.length; i < ilength; i++) {
            parent = arr[i];
            child = parent.children;

            for (j = 0, jlength = child.length; j < jlength; j++) {
                if (input.indexOf(child[j]) > -1) {
                    path = settings.projectPath + cartridge + settings.sass.path.src + parent.parent + '.scss';
                    path = path.replace(/\//g, '\\');
                    mess = child[j] + '.scss' + ' is the part of ' + parent.parent + '.scss';

                    this.messageFn(mess, 'Partials');
                    break;
                }
            }
        }
        if (path === '') {
            path = input;
        }
        return path;
    },
    notifyHandler: function(message, title) {
        return {
            title: title ? title : this.notifyInfo.title,
            icon: this.notifyInfo.icon,
            message: message
        }
    },
    buildWatchPaths: function() {
        var i, iLength, key, keyCartridges, j, jLength, str,
            obj = settings.watch,
            objArr = Object.keys(obj);

        for (i = 0, iLength = objArr.length; i < iLength; i++) {
            key = objArr[i];
            keyCartridges = obj[key].cartridges;
            for (j = 0, jLength = keyCartridges.length; j < jLength; j++) {
                str = settings.projectPath + keyCartridges[j] + obj[key].foldedPath + obj[key].extension;
                this.watchPaths.push(str);
            }
        }
    },
    buildSassPaths: function() {
        var i, iLength, sassSrc;

        for (i = 0, iLength = settings.sass.cartridges.length; i < iLength; i++) {
            sassSrc = settings.projectPath + settings.sass.cartridges[i] + settings.sass.path.src + settings.sass.extension,

            this.sassPaths.push(sassSrc);
        }
    },
    upload: function() {
        var fullWebdavPath = this.webdav.path + this.inputPath,
            name = this.lastElement(this.input, '\\');

        return gulp
            .src(this.input, {read: false})
            .pipe(shell([
                settings.curl + ' --insecure -T ' + this.input +' -u ' + this.webdav.auth + ' ' + fullWebdavPath,
                'echo "-----------------------------------------------------------"',
                'echo File uploaded: ' + name,
                'echo "-----------------------------------------------------------"',
                'echo Uploaded path: ' + fullWebdavPath,
                'echo "-----------------------------------------------------------"',
                'echo Upload is finished'
            ]))
            .pipe(notify(this.notifyHandler(this.message(name, 'upload', 'uploaded'))));
    }
},
plumberErrorHandler =  {
    errorHandler: notify.onError(configuration.notifyHandler(configuration.notifyInfo.errorMessage))
};

configuration.init();

// -----------------------------------------------------------------------------
// Sass compilation
// -----------------------------------------------------------------------------
gulp.task('sass', function () {
    return gulp.src(configuration.sassPathSrc)
        .pipe(plumber(plumberErrorHandler))
        .pipe(sass())
        .pipe(gulp.dest(configuration.sassPathDest));
});

// -----------------------------------------------------------------------------
// Upload to the sandbox changed css files only
// -----------------------------------------------------------------------------
gulp.task('upload', function () {
    return configuration.upload();
});

gulp.task('sass-upload', ['sass'], function () {
    return configuration.upload();
});
// -----------------------------------------------------------------------------
// Watchers
// -----------------------------------------------------------------------------
gulp.task('watch', function() {
    watch(configuration.watchPaths)
        .on('change', function(event) {
            configuration.uploadAction(event, 'change');
        })
        .on('add', function(event) {
            configuration.uploadAction(event, 'add');
        });

    watch(configuration.sassPaths)
        .on('change', function(event) {
            configuration.sassAction(event, 'change');
        });
});

// -----------------------------------------------------------------------------
// Production build
// -----------------------------------------------------------------------------
gulp.task('prod', function (s) {
    let cartridge = s ? s : settings.sass.cartridges[0],
        sassProdSrc = settings.projectPath + cartridge + settings.sass.path.src + settings.sass.extension,
        sassProdDest = settings.projectPath + cartridge + settings.sass.path.dest,
        sassProdDoc = settings.projectPath + cartridge + settings.sass.path.doc;

    return gulp.src(sassProdSrc)
        .pipe(plumber(plumberErrorHandler))
        // .pipe(sassdoc({
        //     dest: sassProdDoc
        // }))
        .pipe(sass())
        .pipe(notify(configuration.notifyHandler(configuration.notifyInfo.compiledMessage, 'Sass')))
        .pipe(autoprefixer(configuration.autoprefixerOptions))
        .pipe(csscomb())
        .pipe(gulp.dest(sassProdDest));

});

// -----------------------------------------------------------------------------
// Build SASS partials tree
// -----------------------------------------------------------------------------
gulp.task('partials', function () {
    let c, cLength;
    for (c = 0, cLength = settings.sass.cartridges.length; c < cLength; c++) {
        configuration.partials[settings.sass.cartridges[c]] = [];
    }
    gulp.src(configuration.sassPaths)
        .pipe(data(function(file) {
            let content, partials, i, iLength, partialsName, gObj, obj, jsonFile, j, jLength,
                filePath = file.path,
                name = path.basename(filePath),
                re = /^_.*/m,
                reImport = /@import\s[\'\"].*?\_([\s\S]*?)[\'\"];/gmi,
                reImportName = /@import\s[\'\"].*?(\_[\s\S]*?)\.scss[\'\"];/m,
                reCartridge = /cartridges\\([\s\S]*?)\\cartridge/m,
                filePathArr = filePath.match(reCartridge),
                cartridge = filePathArr[1];

            if (!re.test(name)) {
                    content = fm(String(file.contents));
                    partials = content.body.match(reImport);

                    obj = {
                        parent: name.replace('.scss', ''),
                        children: []
                    };

                    for (
                        i = 0, iLength = partials.length;
                        i < iLength;
                        i++
                        )
                    {
                        partialsName = reImportName.exec(partials[i]);
                        obj.children.push(partialsName[1]);
                    }
                configuration.partials[cartridge].push(obj);
            }
        }))
        .pipe(through2.obj(function (file, encoding, cb) {
            let jsonFile = new gutil.File({
                path: configuration.partialsJSON,
                contents: new Buffer(JSON.stringify(configuration.partials))
            });
            this.push(jsonFile);
            cb();
        }))
        .pipe(gulp.dest('./'));
});

// -----------------------------------------------------------------------------
// Default task
// -----------------------------------------------------------------------------
gulp.task('default', ['watch']);
